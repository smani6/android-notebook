package com.vendar.email;

/*
 * information of an email account
 */
public class EMailAccount {
	public String UserName = null;
	public String Password = null;
	public String SmtpHost = null;
	public String SmtpPort = null;
	
	public EMailAccount(){
		
	}
	
	public EMailAccount(String username,String password,String smtphost,String smtpport){
		this.UserName = username;
		this.Password = password;
		this.SmtpHost = smtphost;
		this.SmtpPort = smtpport;
	}
}
