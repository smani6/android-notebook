package com.vendar.oauth;

/*
 * user info of open-auth
 */
public class OAuthAccount {
	public String Vendor = null;
	public String UserId = null;
	public String AccessToken = null;
	public String AccessSecret = null;
	
	public OAuthAccount(){
	}
	
	public OAuthAccount(String vendor,String userid,String token,String secret){
		this.Vendor = vendor;
		this.UserId = userid;
		this.AccessToken = token;
		this.AccessSecret = secret;
	}
}
