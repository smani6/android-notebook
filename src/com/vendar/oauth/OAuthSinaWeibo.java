package com.vendar.oauth;

public class OAuthSinaWeibo extends OAuthHelper {
	public static final String WEIBO_VENDOR = "sina_weibo";
	
	public static final String APP_KEY = "717367319";
	public static final String APP_SECRET = "6e763f3532a7b935b9eb52f41120bccb";
	
	public static final String REQUEST_TOKEN_URL = "http://api.t.sina.com.cn/oauth/request_token";
	public static final String ACCESS_TOKEN_URL = "http://api.t.sina.com.cn/oauth/access_token";
	public static final String AUTHORIZE_URL = "http://api.t.sina.com.cn/oauth/authorize";
	
	public OAuthSinaWeibo() {
		super(APP_KEY, APP_SECRET);//SinaWeibo App QuickNote
		mReady = super.SetupAppEngine(REQUEST_TOKEN_URL,ACCESS_TOKEN_URL,AUTHORIZE_URL);
	}
}
