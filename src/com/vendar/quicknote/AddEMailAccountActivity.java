package com.vendar.quicknote;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class AddEMailAccountActivity extends Activity implements OnClickListener {
	private Spinner mEmail = null;
	private EditText mAccount = null;
	private EditText mPassword = null;
	private EditText mSmtpHost = null;
	private EditText mSmtpPort = null;
	private Button mAdd = null;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.add_email_account);
		Setup();
	}
	
	private void Setup(){
		mEmail = (Spinner)findViewById(R.id.email_spinner);
		mAccount = (EditText)findViewById(R.id.account_editor);
		mPassword = (EditText)findViewById(R.id.password_editor);
		mSmtpHost = (EditText)findViewById(R.id.smtp_host_editor);
		mSmtpPort = (EditText)findViewById(R.id.smtp_port_editor);
		mAdd = (Button)findViewById(R.id.add_btn);
		
		mEmail.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				String[] hosts=getResources().getStringArray(R.array.email_smtp_hosts);
				String[] ports=getResources().getStringArray(R.array.email_smtp_ports);
				AddEMailAccountActivity.this.mSmtpHost.setText(
					position<hosts.length?hosts[position]:"");
				AddEMailAccountActivity.this.mSmtpPort.setText(
					position<ports.length?ports[position]:"");
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//nothing todo...
			}
		});
		mAdd.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if( v==mAdd ){
			String account = mAccount.getText().toString();
			if( account == null || account.length()<4 ){
				Toast.makeText(this, R.string.account_cannot_be_null, 500).show();
				return ;
			}
			String password = mPassword.getText().toString();
			if( password == null || password.length()<1 ){
				Toast.makeText(this, R.string.password_cannot_be_null, 500).show();
				return ;
			}
			String smtpHost = mSmtpHost.getText().toString();
			if( smtpHost == null || smtpHost.length()<1 ){
				Toast.makeText(this, R.string.smtp_host_cannot_be_null, 500).show();
				return ;
			}
			String smtpPort = mSmtpPort.getText().toString();
			if( smtpPort == null || smtpPort.length()<1 ){
				Toast.makeText(this, R.string.smtp_port_cannot_be_null, 500).show();
				return ;
			}
			Bundle extras = new Bundle();
			extras.putString("username", account);
			extras.putString("password", password);
			extras.putString("smtp_host", smtpHost);
			extras.putString("smtp_port", smtpPort);
			Intent intent = new Intent();
			intent.setClass(AddEMailAccountActivity.this,StartActivity.class);
			intent.putExtras(extras);
			startActivity(intent);
			finish();
		}
	}
}
