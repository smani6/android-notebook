package com.vendar.quicknote;

import java.sql.Date;
import java.text.SimpleDateFormat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class NoteDbManager {
	
	public static final String DB_NAME = "notebook.db";
	public static final String DB_NOTE_TABLE = "notes";
	
	public static final int DB_VERSION = 2;
	
	public static final String KEY_ID = "_id";
	public static final String KEY_CONTENT = "content";
	public static final String KEY_DATE = "date";
	public static final String KEY_ADDRESS = "address";
	
	private NoteDbHelper mDbHelper = null;
	private SQLiteDatabase mDb = null;
	private Context mContext = null;
	
	public NoteDbManager(Context context)
	{
		this.mContext=context;
		mDbHelper = new NoteDbHelper(mContext);
		mDb = mDbHelper.getWritableDatabase();
	}
	
	//Create a new note
	public long CreateNote(String note)
	{
		ContentValues cvals = new ContentValues();
		cvals.put(KEY_CONTENT, note);
		cvals.put(KEY_DATE,GetCurrentDateString());
		cvals.put(KEY_ADDRESS, GetCurrentAddressString());
		return mDb.insert(DB_NOTE_TABLE, null, cvals);
	}
	
	//delete a note
	public boolean DeleteNote(long id)
	{
		return (mDb.delete(DB_NOTE_TABLE, KEY_ID+"="+id, null)>0);
	}
	
	//update the note
	public boolean UpdateNote(long id,String note)
	{
		ContentValues args = new ContentValues();
		args.put(KEY_CONTENT, note);
		args.put(KEY_DATE,GetCurrentDateString());
		args.put(KEY_ADDRESS,GetCurrentAddressString());
		return (mDb.update(DB_NOTE_TABLE, args, KEY_ID+"="+id, null)>0);
	}
	
	//retrive note
	public Cursor GetNote(long id) throws SQLException
	{
		Cursor cur = null;
		try{
			cur = mDb.query(DB_NOTE_TABLE, 
					new String[]{KEY_ID,KEY_CONTENT,KEY_DATE,KEY_ADDRESS},
					KEY_ID+"="+id, null, null, null, null);
		}catch(Exception e){
			return null;
		}
		if ( cur != null )
		{
			cur.moveToFirst();
		}
		
		return cur;
	}
	
	public Cursor GetNotes() throws SQLException
	{
		return mDb.query(DB_NOTE_TABLE,
				new String[]{KEY_ID,KEY_CONTENT,KEY_DATE,KEY_ADDRESS},
				null, null, null, null, null);
	}
	
	/*
	 * database access interface
	 */
	class NoteDbHelper extends SQLiteOpenHelper
	{
		public NoteDbHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("create table " +
					DB_NOTE_TABLE + " (" + 
					KEY_ID + " integer primary key autoincrement," +
					KEY_CONTENT + " text not null," + 
					KEY_DATE + " text not null," + 
					KEY_ADDRESS + " text not null);");
		}
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			//drop old then create new one
            db.execSQL("DROP TABLE IF EXISTS " + DB_NOTE_TABLE);
            onCreate(db);
		}
	}
	
	/*
	 * generate current time as string.
	 */
	public static String GetCurrentDateString(){
		return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").
		format(new Date(System.currentTimeMillis()));		
	}
	
	/*
	 * get current geography address.
	 */
	public static String GetCurrentAddressString(){
		return "Unknown";
	}
}
