package com.vendar.quicknote;

import com.vendar.quicknote.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class NoteEditor extends Activity {
	
	private static final int OPTION_MENU_GROUP = 1;
	private static final int OPTION_MENU_ITEM_COMPLETE = 1;
	private static final int OPTION_MENU_ITEM_CANCEL = 2;
	private EditText mEditor=null;
	private long mNoteId = -1;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_editor);
        
        mEditor = (EditText)findViewById(R.id.noteEditor);
        Bundle extras = getIntent().getExtras();
        if(extras != null)
        {
        	mNoteId = extras.getLong("id");
        	mEditor.setText(extras.getString("note"));
        }
    }

    //Option Menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(OPTION_MENU_GROUP, OPTION_MENU_ITEM_COMPLETE, 0, R.string.complete_note);
		menu.add(OPTION_MENU_GROUP, OPTION_MENU_ITEM_CANCEL, 0, R.string.cancel);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if( item.getGroupId() == OPTION_MENU_GROUP )
		{
			switch( item.getItemId() )
			{
			case OPTION_MENU_ITEM_COMPLETE:
			{
				Intent it = new Intent();
				Bundle data = new Bundle();
				data.putString("note", NoteEditor.this.mEditor.getText().toString());
				if( mNoteId != (long)-1 )
				{
					data.putLong("id", mNoteId);
				}
				it.putExtras(data);
				NoteEditor.this.setResult(RESULT_OK, it);
				NoteEditor.this.finish();
				break;
			}
			case OPTION_MENU_ITEM_CANCEL:
			{
				NoteEditor.this.setResult(RESULT_CANCELED);
				NoteEditor.this.finish();
				break;
			}
			}
		}
		return true;
	}
}
