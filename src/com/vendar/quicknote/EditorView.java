package com.vendar.quicknote;

import com.vendar.quicknote.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditorView extends EditText{
	private float mEditLineStrokeWidth = 1.0f;
	private int mEditLineOffset = 0;
	public EditorView(Context context) {
		super(context);
	}
    public EditorView(Context context, AttributeSet attrs) {
    	super(context, attrs);
    }

    public EditorView(Context context, AttributeSet attrs, int defStyle) {
    	super(context, attrs, defStyle);
    }

	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);
		//TODO: make variable as parameters defined in layout file
		int lh = super.getLineHeight();
		int lc = super.getLineCount();
		int w = super.getWidth();
		int h = super.getHeight();
		Paint paint = new Paint();
		paint.setColor(getResources().getColor(R.color.note_content_editline));
		paint.setStrokeWidth(mEditLineStrokeWidth);
		int i=0;
		int y = lh;
		for(i=0;i<lc;++i){
			canvas.drawLine(mEditLineOffset, y, w-2*mEditLineOffset, y, paint);
			y += lh;
		}
		while( y < h ){
			canvas.drawLine(mEditLineOffset, y, w-2*mEditLineOffset, y, paint);
			y += lh;
		}
	}
}
